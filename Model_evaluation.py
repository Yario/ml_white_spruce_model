#!/usr/bin/env python3
# -*- coding: utf-8 -*-
''' --------------------------------------------------------------------------
Evaluation and ploting functions for the white spruce machine learning models
---------------------------------------------------------------------------'''

import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression as linreg2
from sklearn.metrics import r2_score
from sklearn.ensemble import GradientBoostingRegressor as BRT
# from scipy.ndimage.filters import gaussian_filter1d # smoothing function
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages # multi page pdf plots
from matplotlib import cm # color maps
from matplotlib.colors import Normalize as color_norm
from matplotlib import figure
from random import choice
import os
import shap
import gc
from string import ascii_lowercase as alphabet
import copy
from calendar import month_name


'''-------------------------------------------------------------------------'''
def plot_clim_hist_over_time(
    save_dir, 
    future_climate, 
    var_name,
    decision_thresholds = None
    ):  
  '''Plot histograms of the climate variables in each time period and scenario
  to assess to what extent the new data is outside the training data and thus
  an extrapolation into the unknown.'''

  fig, axs = plt.subplots(nrows=4, ncols=3, dpi=300, figsize=(16.5, 10))
  x_range= ( min(future_climate[var_name]), max(future_climate[var_name]) )
  baseline_data = future_climate[var_name][future_climate.period == '1961-1990']
  baseline_range = (min(baseline_data), max(baseline_data))
  baseline_quantile =  (np.quantile(baseline_data, 0.05), np.quantile(baseline_data, 0.95) )
  
  for j,ax in enumerate(axs.reshape(-1)): 
    p = np.unique(future_climate.period)[j]
    i = future_climate.period == p
    plot_data = future_climate[var_name][i]

    # fraction of the data that is beyond the (min/max of the ) baseline data 
    quantile_fraction = sum(plot_data.values < [0]) + sum(plot_data.values > baseline_quantile[1])
    quantile_fraction = "beyond 5/95% quantiles: " + str( np.round( quantile_fraction / len(plot_data) * 100, 2) ) + "%"
    # fraction of the data that is beyond 5 and 95% quantile
    extrapolation_fraction = sum(plot_data.values < baseline_range[0]) + sum(plot_data.values > baseline_range[1])
    extrapolation_fraction = "beyond min/max: " + str( np.round( extrapolation_fraction / len(plot_data) * 100, 2) ) + "%"

    temp_name = p.replace("13GCMs_ensemble_","").replace(".gcm","")
      # plt.subplot(nrows=2, ncols=4, index=j)
    ax.set_title(temp_name)
    ax.hist(plot_data, bins=50,  label=temp_name, density=True,
            color="steelblue")#viridis_periods[j] )
    ax.hist(baseline_data, bins=50, histtype="step", color="black", density=True)
    ax.set_xlim(x_range)
    ax.text(0.95,0.85, extrapolation_fraction, transform = ax.transAxes, horizontalalignment='right')
    ax.text(0.95,0.75, quantile_fraction, transform = ax.transAxes, horizontalalignment='right')
    ax.set_xlabel(var_name)
    ax.set_ylabel("Frequency")
    
    # Plot legend in the first panel
    if j == 0:
      import matplotlib.patches as mpatches
      blue_patch = mpatches.Patch(color='steelblue', label='Scenario')
      black_patch = mpatches.Patch(color='black', label='Baseline 1961-1990')
      red_patch = mpatches.Patch(color='red', label='Decision tree')
      
      ax.legend(handles=[blue_patch, black_patch, red_patch], loc="center right")
    
      # Plot decison tree thresholds if available:
    if decision_thresholds is not None:
      plot_thresholds = decision_thresholds.threshold[decision_thresholds.variable_name == var_name]
      # plt.vlines(plot_thresholds, ymin=800, ymax=1000, color=(0,0,0,0.005))
      ax.hist(plot_thresholds, density=True, histtype="step", color="red", bins=50)
  fig.suptitle(var_name.upper(), fontsize=16) 
  fig.subplots_adjust(top=0.88)
  plt.tight_layout()
  fig.savefig(save_dir + "clim_hist_" + var_name + ".png",
              bbox_inches='tight', dpi=300)
  
'''-------------------------------------------------------------------------'''  
def plot_tree_predictions(labels, predictions, metad, predictions_2=None):
  '''Plot predictions for a randomly sampled tree'''
  
  m= 1000 # y-axis maximum
  id_sample = np.random.choice(metad['id'], size=1)[0]
  i = np.where(metad['id'] == id_sample)
  if predictions_2 is not None:
      n_plots = 3
  else:
      n_plots=2
  fig, axs = plt.subplots(n_plots, 1, constrained_layout=True)# initiate panel figure
  fig.suptitle(id_sample) 
  # sort by year:
  i = i[0][np.argsort(metad['year'].iloc[i])]  
  # Plot 1) RW values vs. year
  axs[0].plot(metad['year'].iloc[i], labels.iloc[i], label="measured")
  axs[0].plot(metad['year'].iloc[i], predictions[i], c='red', label="predicted")
  if predictions_2 is not None:
    axs[0].plot(metad['year'].iloc[i], predictions_2[i], c='purple', label="predicted_2")
  # axs[0].axes.set_ylim([0,m])
  axs[0].legend(loc='upper right')
  
  # Plot 2) Measured vs. predicted values for the first predictions (typically the ANN)
  limo = linreg2(fit_intercept=False)
  limo = limo.fit([labels.iloc[i].to_numpy()], [predictions[i]])
  limo_predictions = limo.predict([labels.iloc[i].to_numpy()]).reshape(-1,1)
  r2 = r2_score(labels.iloc[i].to_numpy(), limo_predictions )
  r2 = r2.round(3)
  axs[1].scatter(labels.iloc[i], predictions[i])
  axs[1].axes.set_xlim([0,m])
  axs[1].axes.set_ylim([0,m])
  axs[1].set_title("R² = " + str(r2))
  # Add diagional line for theoretical perfect model
  diagonal = np.linspace(0,m,100)
  axs[1].plot(diagonal, diagonal, '--')
  
  # Plot 3) Measured vs. predicted values for the second predictions (typically the BRT)
  if predictions_2 is not None:
      limo = linreg2(fit_intercept=False)
      limo = limo.fit([labels.iloc[i].to_numpy()], [predictions_2[i]])
      limo_predictions = limo.predict([labels.iloc[i].to_numpy()]).reshape(-1,1)
      r2 = r2_score(labels.iloc[i].to_numpy(), limo_predictions )
      r2 = r2.round(3)
      axs[2].scatter(labels.iloc[i], predictions_2[i], c='purple')
      axs[2].axes.set_xlim([0,m])
      axs[2].axes.set_ylim([0,m])
      axs[2].set_title("R² = " + str(r2))
      # Add diagional line for theoretical perfect model
      axs[2].plot(diagonal, diagonal, '--')
  
'''-------------------------------------------------------------------------'''
def plot_site_eval(d,
                   method, file_name,
                   shap_values_m2b_local,
                   shap_values_m4b_local,
                   omit_var_names,
                   target_name,
                   target_unit,
                   site_dict=None):
  ''' Evaluate the models per site:
  1) Sample size over time
  2) Raw chronology and prediction    
  3) BRT predicted vs. measured + R²
  4) ANN predicted vs. measured + R²
  5) SAHP values BRT
  6) SHAP values ANN
  '''
  
  # --------------------------------------------------------------------------
  #Unit-test settings:
  # d = chron_brt#all_chron,
  # method =  'all' # or #'example' for random single site
  # file_name = "Test_random_site_predictions.pdf"
  # # shap_values_m2b_local = shap_values_m2b_local
  # shap_values_m2b_local = None
  # shap_values_m4b_local = shap_values_m4b_local
  # omit_var_names = tree_var_names
  # target_name = target_name
  # target_unit = target_unit
  # site_dict = site_dict
  # --------------------------------------------------------------------------
  
  sites = list(np.unique(d['site'].astype("str")))
  # Find all different predictions in the dataframe
  pred_names = ["prediction" in cols for cols in d.columns]
  pred_names = d.columns.values[pred_names]
  pred_names_original = pred_names[np.where(["fix" not in x for x in pred_names])]
  # pred_names_fix = pred_names[np.where(["fix" in x for x in pred_names])]
 
  # Plot one random site instread off all sites in a loop
  if method == 'example': sites = [choice(sites)]
  # Maximum ring width values for axis limits
  # ylim_max_all = np.round(site_data['mean_rw/bai'].max(),0).astype(int)*2 # axis max value 
  ylim_max_all = d.labels.quantile(0.95) # for RW 3
  cmap = cm.get_cmap('Paired', 128) # Color maps
  train_color =cmap(0.15)
  validation_color = cmap(0.8)
  test_color = cmap(0.3)
  prediction_color_1 = cmap(0.45)
  prediction_color_2 = cmap(0.66)
  lw1 = 1             # line width
  lw2 = 0.6
  ps2 = 3             # point size
  legend_size = 8
  fs = (14,7)#(19,11) # figure size
  
  # convert this column's type in main script!?-------------------------------
  d.site = d.site.astype(str) # convert this column's type in main script!?
  
  site_names_messy = shap_values_m4b_local.keys()
  for k in site_names_messy:
    if not isinstance(k, str):
      shap_values_m4b_local[str(k)] = shap_values_m4b_local[k]
      shap_values_m4b_local.pop(k)
  #---------------------------------------------------------------------------
  with PdfPages(file_name) as pdf:
    for counter, site in enumerate(sites):
      # counter = 0
      # site = "171"
      plt.interactive(False)
      print(site + " " + str(counter+1) + "/" + str(len(sites)) )
      fig = figure.Figure(figsize=fs, dpi=300)
      model_n = len(pred_names_original) # number models, used for panel size
      # Data subset      
      site_data = d[d['site'] == site]
      site_data = site_data.sort_values(by='year')
      site_train_i = site_data.subset == 0
      site_validation_i = site_data.subset == 1
      site_test_i = site_data.subset == 2
      
      '''01- Sample size over time '''
      plt_i_counter = 1
      plt.subplot(2, 2*model_n, plt_i_counter)
      plt.plot(site_data['year'], site_data['N'])
      plt.xlabel("Year")
      plt.ylabel("Sample size")
      if not site_dict is None:
        # plt.title('a \n' + site_dict[site][0] +  ' ' + site_dict[site][1], size=8) # data origin author name
        plt.title('a \n' + 'Site:' + site_dict[site][1], size=legend_size) # site name only
        plt.title(list(alphabet)[plt_i_counter - 1] + '\nSite: ' + site_dict[site][1], size=legend_size) # site name only
      else:
        # plt.title('a \n Site: ' + site)
        plt.title(list(alphabet)[plt_i_counter - 1] + '\nSite: ' + site, size=legend_size)
      plt.grid()
      plt.tight_layout()
      
      
      ''' 02 - ANN Predicted vs measured mean RW '''
      if( 'ANN_predictions' in site_data.columns):
        plt_i_counter = plt_i_counter + 1
        plt.subplot(2, 2*model_n, plt_i_counter)
        plt.scatter(site_data['labels'][site_train_i], site_data['ANN_predictions'][site_train_i],
                    s=7, color=train_color, rasterized=False)
        plt.scatter(site_data['labels'][site_validation_i], site_data['ANN_predictions'][site_validation_i],
                    s=7, color=validation_color, rasterized=False)
        plt.scatter(site_data['labels'][site_test_i], site_data['ANN_predictions'][site_test_i],
                    s=7, color=test_color, rasterized=False)
        plt.grid()
        # R² values
        ann_r2 = {"train":round(r2_score(site_data['labels'][site_train_i],
                                   site_data['ANN_predictions'][site_train_i]),2) }
        ann_r2["validation"] = round(r2_score(site_data['labels'][site_validation_i],
                                        site_data['ANN_predictions'][site_validation_i]),2)
        ann_r2["test"] = round(r2_score(site_data['labels'][site_test_i],
                                  site_data['ANN_predictions'][site_test_i]),2)
        plt.legend(labels=['%s, R²= %s' % (key, value) for (key, value) in ann_r2.items()], prop={'size': legend_size})
        max_y = max(site_data['labels'][site_train_i]) * 1.2
        plt.plot([0,max_y],[0,max_y], color="grey") #diagonal line (perfect fit)
        plt.xlabel("Measured chronology " + target_name.upper() + " " + target_unit)
        plt.ylabel("Predicted " + target_name.upper() + " " + target_unit)
        # plt.title('b - ANN', size=8)
        plt.title(list(alphabet)[plt_i_counter - 1] + '\nANN', size=legend_size)
        plt.tight_layout()
      
      ''' 03 - BRT Predicted vs measured mean RW '''
      if( 'BRT_predictions' in site_data.columns):
        plt_i_counter = plt_i_counter + 1
        plt.subplot(2, 2*model_n, plt_i_counter)
        plt.scatter(site_data['labels'][site_train_i], site_data['BRT_predictions'][site_train_i],
                    s=7, color=train_color, rasterized=False)
        plt.scatter(site_data['labels'][site_validation_i], site_data['BRT_predictions'][site_validation_i],
                    s=7, color=validation_color, rasterized=False)
        plt.scatter(site_data['labels'][site_test_i], site_data['BRT_predictions'][site_test_i],
                    s=7, color=test_color, rasterized=False)
        plt.grid()
        # R² values
        brt_r2 = {"train":round(r2_score(site_data['labels'][site_train_i],
                                   site_data['BRT_predictions'][site_train_i]),2) }
        brt_r2["validation"] = round(r2_score(site_data['labels'][site_validation_i],
                                        site_data['BRT_predictions'][site_validation_i]),2)
        brt_r2["test"] = round(r2_score(site_data['labels'][site_test_i],
                                  site_data['BRT_predictions'][site_test_i]),2)
        plt.legend(labels=['%s, R²= %s' % (key, value) for (key, value) in brt_r2.items()], prop={'size': legend_size})
        max_y = max(site_data['labels'][site_train_i]) * 1.2
        plt.plot([0,max_y],[0,max_y], color="grey") #diagonal line (perfect fit)
        # plt.xlim(0,ylim_max_all); plt.ylim(0,ylim_max_all)
        plt.xlabel("Measured "+ target_name.upper() + " in " + target_unit)
        plt.ylabel("Predicted "+ target_name.upper() + " in " + target_unit)
        # plt.title('c - BRT', size=8)
        plt.title(list(alphabet)[plt_i_counter - 1] + '\nBRT', size=legend_size)
        plt.tight_layout() 
        
      ''' 04 - Predicted vs. measured as time series, y-axis limited'''
      plt_i_counter = plt_i_counter + 1
      plt.subplot(2, 2*model_n, plt_i_counter)
      plt.plot(site_data['year'], site_data['labels'], linewidth=lw1, c=train_color)
      
      if( 'ANN_predictions' in site_data.columns):
        plt.plot(site_data['year'], site_data["ANN_predictions"],
                 linewidth=lw2, color=prediction_color_1, linestyle="-")
      if( 'BRT_predictions' in site_data.columns):
        plt.plot(site_data['year'], site_data["BRT_predictions"],
                 linewidth=lw2, color=prediction_color_2, linestyle="-")
     
      plt.scatter(site_data['year'][site_validation_i]
                  # [0.1]*sum(site_validation_i)
                  ,site_data['labels'][site_validation_i]
                  ,s=ps2, color=validation_color, zorder=10)
      plt.scatter(site_data['year'][site_test_i]
                  ,site_data['labels'][site_test_i]
                  ,s=ps2, color=test_color, zorder=10)
        
      plt.ylim(0,ylim_max_all)#*2.5
      plt.xlabel("Year")
      plt.ylabel(target_name.upper() + " in " + target_unit)
      labels_temp = ["measured"]
      if( 'ANN_predictions' in site_data.columns):
        labels_temp = labels_temp + ["ANN prediction"]
      if( 'BRT_predictions' in site_data.columns):
        labels_temp = labels_temp + ["BRT prediction"]
      plt.legend(labels=labels_temp,
                 prop={'size': legend_size})
      plt.title(list(alphabet)[plt_i_counter - 1] + '\nRaw chronologies', size=legend_size)
      plt.grid()
      plt.tight_layout()

      ''' 05- Plotting the SHAP values for the ANN; climate variables only '''
      if( 'ANN_predictions' in site_data.columns):
        plt_i_counter = plt_i_counter + 1
        plt.subplot(2, 2*model_n, plt_i_counter)  
        var_subset = [v not in omit_var_names for v in shap_values_m2b_local[site]['feature_names']]    
        shap.summary_plot(shap_values_m2b_local[site]['shap'][:,var_subset],
                          shap_values_m2b_local[site]['variables'][:,var_subset],
                          shap_values_m2b_local[site]['feature_names'][var_subset],
                          show=False,
                          plot_size=fs)
        # plt.title('g\nANN SHAP values', size=8)
        plt.title(list(alphabet)[plt_i_counter - 1] + '\nANN SHAP values', size=legend_size)
        plt.yticks(fontsize=8)
        # Workaround to change axis label font size:
        fig, ax = plt.gcf(), plt.gca()
        ax.set_xlabel("SHAP value (impact on model output)", fontsize=10)
        plt.tight_layout()

      ''' 08 - Plotting the SHAP values for the ANN; climate variables only '''
      if( 'BRT_predictions' in site_data.columns):
        plt_i_counter = plt_i_counter + 1
        plt.subplot(2, 2*model_n, plt_i_counter)
        var_subset = [v not in omit_var_names for v in shap_values_m4b_local[site]['feature_names'] ]    
        shap.summary_plot(shap_values_m4b_local[site]['shap'][:,var_subset],
                          shap_values_m4b_local[site]['variables'][:,var_subset],
                          shap_values_m4b_local[site]['feature_names'][var_subset],
                          show=False,
                          plot_size=fs,
                          color_bar_label='Variable value'
                          )
        plt.gcf().axes[-1].set_aspect(100)
        plt.gcf().axes[-1].set_box_aspect(100)
        plt.title('h\nBRT SHAP values', size=8)
        plt.title(list(alphabet)[plt_i_counter - 1] + '\nBRT SHAP values', size=legend_size)
        plt.yticks(fontsize=8)
        # Workaround to change axis label font size:
        ax = plt.gca()
        ax.set_xlabel("SHAP value (impact on model output)", fontsize=10)
        # ax.set_ylabel("Feature value", fontsize=10)
        # ax.yaxis.set_label_position("right")
        plt.tight_layout()


      #-----------------------------------------------------------------------
      plt.gcf().subplots_adjust(bottom=0.15)
      pdf.savefig()
      ## Avoid memory leak in pyplot in the loop over all sites:
      ## Also mind the figure.Figure(...) command at the start of each loop cycle
      #https://github.com/matplotlib/matplotlib/issues/8560/
      #https://stackoverflow.com/questions/2364945/matplotlib-runs-out-of-memory-when-plotting-in-a-loop
      #https://stackoverflow.com/questions/31156578/matplotlib-doesnt-release-memory-after-savefig-and-close
      plt.close('all')
      plt.pause(0.1)
      gc.collect()

'''-------------------------------------------------------------------------'''
def fixed_tree_size(data, metad, size, train_stats, normalize,
                    new_climate_data=None, size_var_name="ba"):
  ''' Create new datasets for with fixed tree sizes;
  i.e. tree-size does not change over time.
  This allows to assess size specific growth trends. '''
  
  #----------------------------------------------------------------------------
  # # Unit test settings:
  # data = all_data
  # metad = all_metad
  # size = [1000,5000,10000,15000,30000,60000,100000, 140000] # used for basal area = BA
  # train_stats = train_stats
  # new_climate_data = future_climate
  # size_var_name="ba"
  # normalize=False
  #----------------------------------------------------------------------------
  
  if normalize:
    def norm(x):
      return (x - train_stats['mean']) / train_stats['std']
    # un-standardize CRA and CRW to get size/age classes
    def unnorm(x): # destandardization to get the original values back
      return (x * train_stats['std'] + train_stats['mean'])
    crw_cra = unnorm(data)[[size_var_name,"cra"]] 
    metad2 = pd.concat([metad,crw_cra], axis=1)
  else:
    metad2 = pd.concat([metad, data[["ba","cra"]] ], axis=1)

  ''' Find for each tree the closest CRW/BA to each defined CRW-size class'''
  metad2 = metad2.sort_values(by=["site","id","year"]) # quicker to sort here
  def closest_to_crw_class(x, size):
    x_diff = abs(x[size_var_name] - size) # minimum distance to defined size class
    # Check maximum distance from size-class;
    # e.g. to avoid that 110mm is closes to 300mm
    # if min(x_diff) < 5: # 5mm max deviation threshold # previously with CRW used
    ## define max realtive deviation from the size class; not absolute deviation in mm or mm²
    if min(x_diff) < size * 0.05: # 5% maximum deviation threshold
    # if min(x_diff) < (s**0.5 * 0.1 )**2 : # maximum relative DIAMETER deviation
      min_i = x.index[np.argmin(x_diff)] # index of minimum
      return min_i.astype("int")

  mean_site_cra = {}
  for s in size:
    print("computing size class " + str(s))
    temp_i = metad2.groupby(['id'], sort=False).apply(closest_to_crw_class, size=s)
    temp_i = temp_i.dropna()
    metad_subset = metad2.loc[temp_i,:]
    ## compute the mean cra at the size class (crw/ba)
    mean_site_cra[size_var_name + str(s)] = metad_subset.groupby('site').mean() # or better median?
    plt.hist(mean_site_cra[size_var_name + str(s)][size_var_name], 100) # plot histograms
    print( "N = " + str(len(mean_site_cra[size_var_name + str(s)][size_var_name])))
  plt.show() # show histogram
  
  ## Combine the tree data with the original or new climate data
  ''' 1) original climate data '''
  if new_climate_data is None:
    print("Combining with original climate data")
    site_year = metad[["site","year"]].drop_duplicates() # all site and year combinations
    crw_class_data = {}
    crw_class_metad = {}
    for crw_class in mean_site_cra.keys(): # loop over crw/ba classes
      data_new = data[0:0]
      metad_new = pd.DataFrame(columns=["year","site"])
      for s in site_year.site.unique(): # Loop over sites - implement faster version with aggregate!?
        if s in mean_site_cra[crw_class].index:
          i = site_year[site_year.site== s].index
          site_subset = data.loc[i].copy() #  copy climate data subset
          site_subset.cra = mean_site_cra[crw_class].loc[s]["cra"] # replace cra values
          site_subset[size_var_name] = mean_site_cra[crw_class].loc[s][size_var_name] # replace crw/ba values
          site_subset = site_subset.reset_index(drop=True) # reset index
          data_new = data_new.append(site_subset)
          metad_new = metad_new.append(metad.iloc[i][["year","site"]])
    # standardize CRW and CRA again
    data_new.cra = (data_new.cra - train_stats['mean'].cra) / train_stats['std'].cra
    data_new[size_var_name] = (data_new[size_var_name] - train_stats['mean'][size_var_name]) / train_stats['std'][size_var_name]
    crw_class_data[crw_class] = data_new    # Append results to output
    crw_class_metad[crw_class] = metad_new  # Append results to output
    return (crw_class_data, crw_class_metad)
  
  ''' 2) New climate data (e.g. climate change scenarios) ''' # merge all unqiue site+crw/ba+cra data into one dataframe
  if new_climate_data is not None:
    print("Combining with new climate data")
    tree_data = mean_site_cra.copy()
    for i in tree_data.keys():
      tree_data[i]["site"] = tree_data[i].index
      tree_data[i]["size_class"] = i
    tree_data = pd.concat(tree_data).reset_index(drop=True)
    tree_data = tree_data.drop("year", axis=1)
    new_data = new_climate_data.merge(tree_data, how="inner", on="site").reset_index(drop=True)
    new_metad = new_data[["site","year","period","size_class"]]
    new_data = new_data[data.columns]
    ## standardize all values again
    if normalize:
      new_data =   (new_data)
    return new_data, new_metad

'''-------------------------------------------------------------------------'''
def plot_climate_change_growth_trends(predictions, new_metad, target_name,
                                  filename, title=None, ylim=None):
  ''' Compute the mean RW for each size-class, climate-change scenario and time-period.
  Plot the results together in one figure.'''
  new_metad["predictions"] = predictions
  # Group variables
  period_size_means = new_metad.groupby(by = ["period", "size_class"], ).predictions.mean()
  period_size_means2 = period_size_means.reset_index() 
  # Create string that can be used as axis label for the time periods
  period_label = period_size_means2.period.unique()
  period_label = [x.replace(".gcm", "")[-9:] for x in period_label] # extract subsetring
  period_label = list(set((period_label))) # keep only unique values
  period_label.sort()
  # get better scenario name:
  period_size_means2["scenario"] =  [txt.replace("13GCMs_ensemble_","") for txt in period_size_means2.period]
  period_size_means2["scenario"] =  [txt.replace(".gcm","") for txt in period_size_means2["scenario"]]
  period_size_means2["years"] = [txt[-9:] for txt in period_size_means2.scenario]
  period_size_means2 = period_size_means2[['size_class', 'predictions', 'scenario','years']] #drop unnecessary columns
  if target_name == "rw":
    period_size_means2["size_class"] = [int(txt[3:]) for txt in period_size_means2.size_class] # size class as int; for legend order
  if target_name == "bai":
    period_size_means2["size_class"] = [int(txt[2:]) for txt in period_size_means2.size_class] # size class as int; for legend order
  period_size_means2 = period_size_means2.sort_values(by=["size_class", "scenario", "years"])
  period_size_means2 = period_size_means2.reset_index(drop=True)
  # Specify in which order scenarios and time periods will be plottet 
  temp = np.array([1,2,3,4,1,5,6,7,1,8,9,10,1,11,12,13])
  temp = temp - 1
  plot_order = np.array([])
  for i in range(int(len(period_size_means2) / 13)):
    plot_order = np.append(plot_order, [temp + i*13])
  plot_order = plot_order.reshape((int(len(plot_order) / 4),4)).astype(int)
  
  size_classes = period_size_means2.size_class.unique()
  n_size_class = len(size_classes)
  size_colors = cm.get_cmap('magma', 100)(np.linspace(0,0.9,n_size_class))
  color_i = 0
  scenario_i = 0
  x_offset = np.linspace(-0.3, 0.3,4)
  
  plt.figure(figsize=(14/2.54, 10/2.54)) 
  for i in range(len(plot_order)):
    plt.scatter(
      x = scenario_i + 1 + x_offset,
      y = period_size_means2.predictions[plot_order[i]],
      color = size_colors[color_i])
    plt.plot(scenario_i + 1 + x_offset, period_size_means2.predictions[plot_order[i]], color=size_colors[color_i])
    scenario_i = scenario_i + 1
    if (i+1)%4 == 0:
     color_i = color_i + 1
     scenario_i = 0
  
  if ylim is not None:
    ymax = ylim[1]
  else:
    ymax=period_size_means2.predictions.max()
  plt.vlines([1.5,2.5,3.5], ymin=0, ymax=ymax, color="grey")
  plt.xlim(0.5,6)
  if target_name == "rw":
    plt.ylabel("ring width in mm")
    size_var_name = "Diameter\nin mm"
  if target_name == "bai":
    plt.ylabel("BAI in mm²")
    size_var_name = "Basal Area\nin mm²"
  plt.xlabel("Climate change scenario")
  plt.xticks([1,2,3,4], labels=["ssp126", "ssp245", "ssp370", "ssp585"])#, rotation='vertical')
  ## Second x-axis:
  plt.twiny()
  plt.xlim(0.5,6)
  plt.xticks([x + y + 1 for x in range(4) for y in x_offset],#[x + x_offset for x in [1,2,3,4]],
             labels = period_label * 4,
             rotation = 'vertical')
  ## Color legend for the differenr tree-size classes (diameter)
  legend_points = [plt.Line2D([0], [0], marker='.', color=x,  label='Scatter', markerfacecolor=x, markersize=15) for x in size_colors]
  plt.legend(legend_points, size_classes, title=size_var_name)
  # Add figure title if specified:
  if title is not None:
    plt.title(title)
  # Use custopm ylim if specified:
    if ylim is not None:
      plt.ylim(ylim)
  #
  plt.savefig(filename, bbox_inches='tight', dpi=300)

      
'''-------------------------------------------------------------------------'''
def siteR2(file_name, d):
  ''' Calculate the R² between the measured and the predicted site chronologies
  for the ANN as well as the BRT models
  Could be extended to include spline detrended chronologies and "Gleichläufigkeit" '''
  color1= cm.viridis(0)
  color2= cm.viridis(0.5)
  output = pd.DataFrame({'site': d.site.unique()} )
  for i,site in enumerate(output.site):
    j = d.site == site
    j_train = np.logical_and(d.site == site, d.subset==0)
    j_validation = np.logical_and(d.site == site, d.subset==1)
    j_test = np.logical_and(d.site == site, d.subset==2)
    
    # ANN
    if "ANN_predictions" in d.columns:
      output.loc[i,'r2_ann_all'] = r2_score(d.labels[j], d.ANN_predictions[j])
      output.loc[i,'r2_ann_train'] = r2_score(d.labels[j_train], d.ANN_predictions[j_train])
      output.loc[i,'r2_ann_validation'] = r2_score(d.labels[j_validation], d.ANN_predictions[j_validation])
      output.loc[i,'r2_ann_test'] = r2_score(d.labels[j_test], d.ANN_predictions[j_test])
    #BRT
    if "BRT_predictions" in d.columns:
      output.loc[i,'r2_brt_all'] = r2_score(d.labels[j], d.BRT_predictions[j])
      output.loc[i,'r2_brt_train'] = r2_score(d.labels[j_train], d.BRT_predictions[j_train])
      output.loc[i,'r2_brt_validation'] = r2_score(d.labels[j_validation], d.BRT_predictions[j_validation])
      output.loc[i,'r2_brt_test'] = r2_score(d.labels[j_test], d.BRT_predictions[j_test])
  
  ''' Plot 1) R2 based on ALL data as histogram and save as jpg '''
  plt.figure(figsize=(7/2.54, 4/2.54)) 
  bins = np.linspace(-5, 1, 100)
  if "ANN_predictions" in d.columns:
    plt.hist(output.r2_ann_all, bins, alpha=0.7)
  if "BRT_predictions" in d.columns:
    plt.hist(output.r2_brt_all, bins, alpha=0.7)
  plt.xlim([-4,1]) # limit x-axis for nicer plot
  plt.xlabel("Site R²")
  plt.ylabel("Frequency")
  if "ANN_predictions" in d.columns and "BRT_predictions" in d.columns :
    plt.legend(["ANN","BRT"])
  # plt.savefig('Site_chronology_R2.jpg', bbox_inches='tight', dpi=300)
  plt.show()
  plt.close()
  
  ''' Plot 2) R2 separate for train/validation/test data as histogram and save as jpg '''
  fig, ax = plt.subplots(ncols=3, nrows=1, figsize=[14,5])
  
  # Train
  if "ANN_predictions" in d.columns:
    ax[0].hist(output.r2_ann_train, bins, alpha=0.7, color=color1)
  if "BRT_predictions" in d.columns:
    ax[0].hist(output.r2_brt_train, bins, alpha=0.7, color=color2)
  ax[0].set_xlim([-4,1]) # limit x-a0xis for nicer plot
  ax[0].set_ylim([0,100]) # limit y-axis for nicer plot
  ax[0].set_xlabel("Site R²")
  ax[0].set_ylabel("Frequency")
  if "ANN_predictions" in d.columns and "BRT_predictions" in d.columns :
    ax[0].legend(["ANN","BRT"])
  ax[0].set_title("a\nTrain")
  
  # Validation
  if "ANN_predictions" in d.columns:
    ax[1].hist(output.r2_ann_validation, bins, alpha=0.7, color=color1)
  if "BRT_predictions" in d.columns:
    ax[1].hist(output.r2_brt_validation, bins, alpha=0.7, color=color2)
  ax[1].set_xlim([-4,1]) # limit x-axis for nicer plot
  ax[1].set_ylim([0,100]) # limit y-axis for nicer plot
  ax[1].set_xlabel("Site R²")
  ax[1].set_ylabel("Frequency")
  ax[1].set_ylabel("")
  if "ANN_predictions" in d.columns and "BRT_predictions" in d.columns :
    ax[1].legend(["ANN","BRT"])
  ax[1].set_title("b\nValidation")
  ax[1].axes.yaxis.set_ticklabels([])
  
  # Test
  if "ANN_predictions" in d.columns:
    ax[2].hist(output.r2_ann_test, bins, alpha=0.7, color=color1)
  if "BRT_predictions" in d.columns:
    ax[2].hist(output.r2_brt_test, bins, alpha=0.7, color=color2)
  ax[2].set_xlim([-4,1]) # limit x-axis for nicer plot
  ax[2].set_ylim([0,100]) # limit y-axis for nicer plot
  ax[2].set_xlabel("Site R²")
  # ax[2].set_ylabel("Frequency", fontsize=18)
  ax[2].set_ylabel("", fontsize=18)
  if "ANN_predictions" in d.columns and "BRT_predictions" in d.columns :
    ax[2].legend(["ANN","BRT"])
  ax[2].set_title("c\nTest")
  ax[2].axes.yaxis.set_ticklabels([])
  
  plt.savefig(file_name, bbox_inches='tight', dpi=300)
  plt.show()
  plt.close()
  # Return R² values:
  return(output )

'''-------------------------------------------------------------------------'''
def ring_to_chron(labels, predictions, metad, prediction_name="prediction"):
  ''' Calculate site chronologies (average per site and year) '''
  # keeping the subset id (train/validation/test) is a bit annoying but necessary
  column_select = ['site','year','labels',prediction_name,'subset']
  metad2 = metad.copy()
  if labels is not None:
    metad2['labels'] = labels
  else:
    column_select.remove('labels')
  if 'subset' not in metad2.columns:
    column_select.remove('subset')
  metad2[prediction_name] = predictions
  # calculate raw (undetrended) site means (aka. site chronologies)
  if "subset" in metad2.columns:
    metad2.subset = metad2.subset.astype(float)
  site_chrons = metad2[column_select].groupby(['site', 'year']).mean()
  site_chrons['N'] = metad2[ column_select ].groupby(['site', 'year']).count()[column_select[-1]]
  site_chrons = site_chrons.reset_index()
  # subset to integer
  if "subset" in metad2.columns:
    site_chrons.subset = site_chrons.subset.astype("int32")    
  # change name of prediction column
  return site_chrons

'''-------------------------------------------------------------------------'''
def plot_prediction_vs_truth(predictions, labels, subset
                             ,xlab, ylab, title, filename):
  ''' Simple plot of predictions vs. true values for chronologies '''
  viridis = cm.get_cmap('viridis', 4) # subset color
  # Loop over train/validation/test subsets and plot points
  f = plt.figure(figsize=(4,3))
  m = np.round(labels.max(),0).astype(int) # ~5000 for BAI
  r2 = np.array([])
  for i in np.unique(subset):
    j = subset==i
    plt.scatter(x=labels[j], y=predictions[j], s=1, alpha=0.5, color=viridis((i)/4) )
    r2 = np.append(r2, r2_score(labels[j], predictions[j]) )
  r2 = np.around(r2,3)
  plt.grid()
  plt.legend(labels=["Train, R²="+ str(r2[0]),
                     "Validation, R²="+ str(r2[1]),
                     "Test, R²="+ str(r2[2]) ],
             markerscale=5).set_alpha(1)
  plt.plot(range(m), range(m), c='red') # diagonal line for theoretical perfect fit model
  plt.xlabel(xlab)
  plt.ylabel(ylab)
  if len("title") > 0:
    plt.title(title)
  plt.xlim(0, m)
  plt.ylim(0, m)
  
  f.savefig(filename, bbox_inches='tight', dpi=300)
  plt.show()

'''-------------------------------------------------------------------------'''
def compute_shap(model, data, interpretability="global", return_explainer=False,
                 site=None, N=None, sample_weights=None):
  ''' Assess the importance of the input variables of the models with SHAP:
  SHapley Additive exPlanations. See: Lundberg and Lee 2016
  Returns the SHAP values and variable names.
  Either:
      - global: variable importance relative to all datapoints
      - local: variable importance at each site
      (e.g. latitude has little importance at a site because it never changes)
  ''' 
  
  ### For unit test:
  # model = model_4b
  # data = data_prep_m4(train_data, clim_var_names,tree_var_names, return_names=True)
  # interpretability="local"#"global"
  # site = train_metad.site
  # N = 200#5000
  # sample_weights = train_weights

  
  ''' Global interpretation------------------------------------------------'''
  if interpretability == "global":
    
    # Check ift ANN or BRT model was provided and setup accordingly 
    
    ''' DEPRECATED: ANN model global'''
    # if type(model) == tf.python.keras.engine.functional.Functional:
    #   # Unzip data into training data and variable names for plotting
    #   climate_data, tree_data, var_names = data
    #   ''' all data '''
    #   if N == None: # either use all values to compute SHAP (veeery slow)
    #     data2 = [climate_data, np.array(tree_data)]
    #     ''' data subset '''
    #   else: # use a random data subset to compute SHAP (faster)
    #     sample_weights_scaled = sample_weights / sum(sample_weights) # scaled, so they sum up to 1
    #     random_i = np.random.choice(range(data[0].shape[0]), N, p=sample_weights_scaled)
    #     data2 = [climate_data[random_i], np.array(tree_data)[random_i]]
    #   shap_explainer = shap.DeepExplainer(model=model, data=data2)
    #   shap_values = shap_explainer.shap_values(X=data2)
    #   # Reformatting and saving as dictionary (shap values, variable values, variable names)
    #   clim_shap = np.array([np.array(xi) for xi in shap_values[0][0]])
    #   tree_shap = np.array([np.array(xi) for xi in shap_values[0][1]])  
    #   all_shap = np.concatenate((clim_shap[:,:,0], tree_shap), axis=1)
    #   # Create a dictionary with the shap values, the input variable values, and the variable names
    #   shap_values = {'shap' : all_shap,
    #                  'variables' : np.concatenate((data2[0][:,:,0], data2[1]), axis=1),
    #                  'feature_names' : var_names
    #                  }
    
    ''' BRT Model global'''
    if type(model) == BRT:
      data2, var_names = data
      if not N == None: # Only use a data subset if N was specified
        sample_weights_scaled = sample_weights / sum(sample_weights) # scaled, so they sum up to 1
        random_i = np.random.choice(range(len(data2)), N, p=sample_weights_scaled)
        data2 = data2.iloc[random_i,:]
      # Calculate SHAP values
      shap_explainer = shap.TreeExplainer(model=model)#, data=data2)
      shap_values = shap_explainer.shap_values(X=data2, check_additivity=False)
      # Create a dictionary with the shap values, the input variable values, and the variable names
      shap_values = {'shap' : shap_values,
                     'variables' : data2.to_numpy(),
                     'feature_names' : var_names
                     }
  
  ''' Local interpretation - loop over sites-------------------------------'''
  if interpretability == "local":
    all_sites = site.unique()
    
    ''' DEPRECATED: ANN model local'''
    # if type(model) == tf.python.keras.engine.functional.Functional:
    #   # Unzip data into training data and variable names for plotting
    #   climate_data, tree_data, var_names = data
    #   # data2 = [climate_data, np.array(tree_data)]
    #   for i,s in enumerate(all_sites): # loop over sites
    #     print("Site " + str(i+1) + "/"  + str(len(all_sites)) + " " + s)        
    #     site_i = s == site # index for the subset of the data for single site
    #     site_i_numeric = np.where(site_i)
    #     # Additionally, only take a subset of maximum N samples:
    #     if len(site_i_numeric[0]) > N:
    #       site_i_numeric = np.random.choice(site_i_numeric[0], 100)
    #     data_site_subset = [ climate_data[site_i_numeric], np.array(tree_data)[site_i_numeric] ]
    #     # Create SHAP explainer
    #     shap_explainer = shap.DeepExplainer(model=model, data=data_site_subset )
    #     # Compute SHAP values (takes very long), only us a subset
    #     shap_values_site = shap_explainer.shap_values(X=data_site_subset)
    #     # Create a dictionary with the shap values, the input variable values, and the variable names
    #     shap_values_site = {
    #       'shap' : np.concatenate((shap_values_site[0][0][:,:,0], shap_values_site[0][1]), axis=1),
    #       'variables' : np.concatenate((data_site_subset[0][:,:,0], data_site_subset[1]), axis=1),
    #       'feature_names' : var_names
    #       }
    #     # Save in dictionary with site as key; if first site then create dict
    #     if i == 0:
    #       shap_values = {s:shap_values_site}
    #     else:
    #       shap_values[s] = shap_values_site

    ''' BRT model local '''
    if type(model) == BRT:
      data2, var_names = data
      for i,s in enumerate(all_sites): # loop over sites
        print("Site " + str(i+1) + "/"  + str(len(all_sites)) + " " + str(s))
        site_i = s == site # index for the subset of the data for single site
        site_i_numeric = np.where(site_i)
        # Additionally, only take a subset of maximum N samples:
        if len(site_i_numeric[0]) > N:
          site_i_numeric = np.random.choice(site_i_numeric[0], 100)
        data_site_subset = data2[site_i]
        # Create shap explainer
        shap_explainer = shap.TreeExplainer(model=model, data=data_site_subset )
        # Compute SHAP values (takes very long), only us a subset
        shap_values_site = shap_explainer.shap_values(X=data_site_subset, check_additivity=False)
        shap_values_site = {'shap' : shap_values_site,'variables' : data_site_subset.to_numpy(),'feature_names' : var_names }
        # shap_values= shap_values_site
        # Save in dictionary with site as key; if first site then create dict
        if i == 0:
          shap_values = {s:shap_values_site}
        else:
          shap_values[s] = shap_values_site
  if return_explainer:
      return shap_values, shap_explainer
  else:
    return shap_values

'''-------------------------------------------------------------------------'''
def plot_shap(shap_values, filename, title, x_label, omit_var=None):
  ''' Plot the SHAP values as beeswarm plot'''
  
  # get index for subset before formatting the feature names
  if omit_var != None:
    var_subset = [v not in omit_var for v in shap_values['feature_names']]
  
  # formatting the feature names for the plot
  feature_names = list(shap_values['feature_names'])
  feature_names = [temp.replace("_", " ") for temp in feature_names]
  # last two character to month name, if numeric:
  for i in range(len(feature_names)):
    try:
      m_string = feature_names[i][-2:]
      m = month_name[int(m_string)]
      feature_names[i] = feature_names[i].replace(m_string," " + m)
      split_name = feature_names[i].split()
      if split_name[0] == "prev":
        feature_names[i] = split_name[1] +" "+ split_name[0] +" "+ split_name[2]
    except:
      pass
  
  shap_values['feature_names'] = feature_names
 
  if omit_var == None: # Plot with all variables
    shap.summary_plot(shap_values['shap'],
                      shap_values['variables'],
                      shap_values['feature_names'],
                      plot_type="dot",
                      show=False,
                      max_display=10,
                      color_bar_label='Variable value'
                      )

  else: # Plot with selected variables omitted
    shap.summary_plot(shap_values['shap'][:,var_subset],
                      shap_values['variables'][:,var_subset],
                      np.array(shap_values['feature_names'])[var_subset],                      
                      plot_type="dot",
                      show=False,
                      max_display=10,
                      color_bar_label='Variable value',
                      plot_size=0.5
                      )
    
  # fixes issue with SHAP and matplotlib, see: https://stackoverflow.com/questions/70461753/shap-the-color-bar-is-not-displayed-in-the-summary-plot
  plt.gcf().axes[-1].set_aspect(100)
  plt.gcf().axes[-1].set_box_aspect(100)
  
  # customize the x-axis label
  ax = plt.gca()
  ax.set_xlabel(x_label, fontsize=14)
  
  plt.title(title)
  plt.gcf().set_size_inches(12/2.54, 8/2.54)
  plt.savefig(filename, bbox_inches="tight", dpi=300)
  plt.show()

'''-------------------------------------------------------------------------'''
def shap_subset(all_data, all_metad, all_weights, model, data_prep, N,
                clim_var_names, tree_var_names,
                var_name, quantile_breaks, return_shap_values, save_dir=""
                ):
  ''' SHAP values are computed for a 'background dataset'.
  This function creates data subsets (based on quantiles if the  grouping variable is continuous)
  and computes the SHAP values for each subset'''
  
  ## Unit testing:
  # model = model_4b
  # data_prep = data_prep_m4
  # N=20
  # var_name = var_name
  # quantile_breaks = [0.1,0.3,0.5,0.7,0.9]
  # return_shap_values = True
    
  if  not "region" in var_name:
    var_quantiles = all_data[var_name].quantile(quantile_breaks)
    var_quantiles = np.array(var_quantiles)
    n_classes = len(var_quantiles) - 1
    quantile_names = [str(quantile_breaks[i]) +" - "+ str(quantile_breaks[i+1]) for i in range(n_classes)]
  else:
    var_quantiles = all_metad[var_name].unique()
    var_quantiles = var_quantiles[var_quantiles != None] # omit "None"
    n_classes = len(var_quantiles)
    quantile_names = var_quantiles
  quantile_data = list()
  quantile_metad = list()
  quantile_weights = list()
  shap_values_quantile = dict()
  for i in range(n_classes): # looping over quantiles or classes/cluster
    print("Processing subset/quantile: " + quantile_names[i])#str(var_quantiles[i]) )
    # Prepare data for SHAP
    if var_name in all_data.columns:
      qi = (all_data[var_name] >= var_quantiles[i]) & (all_data[var_name] < var_quantiles[i+1])
    else:
      qi = (all_metad[var_name] == var_quantiles[i])
    quantile_data.append( all_data[qi] )
    quantile_metad.append( all_metad[qi] )
    quantile_weights.append( all_weights[qi] ) 
    # Compute SHAP values
    shap_values_quantile[quantile_names[i]] = compute_shap(
      model = model,
      data = data_prep(quantile_data[i], clim_var_names, tree_var_names, return_names=True),
      interpretability="global", 
      N = N,
      sample_weights = quantile_weights[i]
      )
    # Plot SHAP values
    
    if "region" in var_name:
      quantile_name = var_quantiles[i]
    else:
      quantile_name = " quantile " + str(i+1)
    
    # Model name for file name and plot title
    if type(model) == BRT:
      model_name = "BRT"
    ## ANN is deprecated
    # if type(model) == tf.python.keras.engine.functional.Functional:
    #   model_name = "ANN"
    
    plot_shap(shap_values_quantile[quantile_names[i]],
              filename = save_dir + "SHAP_model_"  + model_name + " " + var_name + "_" + quantile_name + "_a.jpg",
              title = model_name + " model, " + var_name + " "  + quantile_name,
              x_label = "SHAP value (effect on BAI in mm²)" )
    plot_shap(shap_values_quantile[quantile_names[i]],
              filename = save_dir + "SHAP_model_" + model_name + " " + var_name + "_" + quantile_name + "_b.jpg",
              title = model_name + " model, " + var_name + " " + quantile_name, omit_var = tree_var_names,
              x_label = "SHAP value (effect on BAI in mm²)" )
    
  if return_shap_values == True:
    return shap_values_quantile

'''-------------------------------------------------------------------------'''
def shap_swarm_plot(x,                  # List(!) of outputs from shap_subset
               selected_variabes,       # the environmental variables that will be plotted for each cluster set in x
               unstandardize,           # if the variables have been standarized (for the ANN, not the BRT) 
               train_stats,            # Train statistics to un-derstandardize the input variables
               target_name,
               target_unit,
               save_dir,
               alpha=1
               ):
    """ Creates a custom swarm plot for all clusters in each SHAP values set
    The plots show the effect that variable x as within the different data subset (ecoregion, quantile)."""
    ## create directory if it does not exist yet
    if not os.path.exists(save_dir):
      os.mkdir(save_dir)
    ## Loop over the grouping variables (e.g. BA or region)
    for clustered_by in x.keys():
        all_cluster = x[clustered_by].keys()
        input_variable_names = np.array(x["Ecoregion"][list(x["Ecoregion"].keys())[0]]["feature_names"])
        '''Merging all SHAP and variable values into a 4D array'''
        # dimensions: 1-sample, 2-variable, 3-shap/inputvariable, 4-cluster
        cluster_shap_array = dict() # first dict, then 4D array
        for cluster in all_cluster:#x["region"].keys():
          cluster_shap_array[cluster] = np.array([x[clustered_by][cluster]["shap"],
                                                  x[clustered_by][cluster]["variables"]] )
          cluster_shap_array[cluster] = np.moveaxis(cluster_shap_array[cluster],0,-1)
        
        cluster_shap_array = np.array(list(cluster_shap_array.values()))
        cluster_shap_array = np.moveaxis(cluster_shap_array,0,3)
        
        for var in selected_variabes:
            var_i = np.where(input_variable_names == var)[0]
            # un-standardize variable/color values for all clusters combined if needed
            if unstandardize:
              color_var_unscaled = cluster_shap_array[:,var_i,1,:] * train_stats['std'][var] + train_stats['mean'][var]
            else:
              color_var_unscaled = cluster_shap_array[:,var_i,1,:]
            color_scaler = color_norm(
              vmin = np.quantile(color_var_unscaled, 0.01),   # omit outlier for better coverage of the color scale
              vmax = np.quantile(color_var_unscaled, 0.99) )
            for cluster_i in range(cluster_shap_array.shape[3]): # loop over cluster
                jitter = np.random.uniform(low=-0.25, high=0.25, size=len(cluster_shap_array)) + 0.5 # jittered coordinates for plot
                plt.scatter(x=cluster_shap_array[:,var_i,0,cluster_i], y = jitter + cluster_i,
                            c = color_var_unscaled[:,0,cluster_i],
                            norm=color_scaler,
                            cmap=cm.viridis,
                            alpha=alpha, s=3
                            )
            plt.xlabel("Effect on " + target_name.upper() + " in " + target_unit)
            plt.ylabel(clustered_by.replace("_", " ").upper() + " Quantiles")
            plt.yticks(ticks = np.array(list(range(cluster_shap_array.shape[3])))+0.5,
                       # np.linspace(0.5, cluster_shap_array.shape[3]+0.5, cluster_shap_array.shape[3]+0.5)
                       labels=all_cluster )
            plt.title(var.replace("_", " ").upper())
            
            color_bar = plt.colorbar(label = var.replace("_", " ").upper() + unit_from_var_name(var), alpha=1)
            color_bar.set_alpha(1)
            color_bar.draw_all()
            
            plt.grid()
            plt.savefig(save_dir + 'SHAP_clustered_by_' + clustered_by + "_variable_" + var + '.jpg', bbox_inches='tight', dpi=300)
            plt.show()


'''-------------------------------------------------------------------------'''
def shap_heatmap_scenario_ecoregion(save_dir,
                                    shap_values, 
                                    tree_var_names, 
                                    clim_var_names,
                                    standardized_color_scale = True
                                    ):
  ''' Aggregate the ShAP values for each scenario and time period to create
  a raster with:\n 
    x-axis = monthly climate variables and\n  
    y-axis = ecoregion,\n 
    next:\n 
    a) plot this raster as heatmap,\n 
    b) compute the differenece to the baseline scenario and plot as heatmap\n 
  
  Parameters
  ----------
  save_dir : directory where the images will be saved, relative from the current directory\n 
  shap_values : the ShAP values that have been computed for each ecoregion, scenario and time period\n 
  tree_var_names : tree and site variable names\n 
  clim_var_names : climate variable names\n 
  '''
  

  ''' Formatting the the ShAP dictionary into matrices
  --------------------------------------------------------------------------'''
  example_ecoregion_name = list(shap_values.keys())[0]
  # get the index of only the monthly climate variables (no site or tree variables)
  clim_var_i = [v not in tree_var_names for v in shap_values[example_ecoregion_name]['feature_names'] ]
  clim_var_i = np.where(clim_var_i)[0]
  x_tick_names =  shap_values[example_ecoregion_name]["feature_names"][clim_var_i]

  y_tick_ecoregion_names = list(shap_values.keys())
  all_scenario_names = np.unique([x.split(" ")[0] for x in y_tick_ecoregion_names])
  y_tick_ecoregion_names = np.unique([x.split(" ",1)[1] for x in y_tick_ecoregion_names])

  '''Order of ticks and tick names
  --------------------------------------------------------------------------'''
  # Sort climate variables chronologically for the heat-map
  x_tick_names_sorted = []
  for i in range(1,13):
    for cvn in clim_var_names:
      x_tick_names_sorted = x_tick_names_sorted + ["prev_" + cvn + f'{i:02d}']
  for i in range(1,9):
    for cvn in clim_var_names:
      x_tick_names_sorted = x_tick_names_sorted + [cvn + f'{i:02d}']      
  
  x_tick_order = []
  for x in x_tick_names_sorted:
    x_tick_order = x_tick_order + [ np.where([x == y for y in x_tick_names])[0][0] ]
    
  # Tick mark name of env. variables without month name (lower x axis)
  x_tick_clim_only = [s.replace("prev_", "") for s in  x_tick_names_sorted]
  x_tick_clim_only = [s[0:4] for s in  x_tick_clim_only]
  x_tick_clim_only = [s.replace("0", "") for s in   x_tick_clim_only ]
  x_tick_clim_only = [s.replace("1", "") for s in   x_tick_clim_only ]
  
  x_tick_clim_only = [s.replace("tave", "T") for s in   x_tick_clim_only ]
  x_tick_clim_only = [s.replace("ppt", "P") for s in   x_tick_clim_only ]
  x_tick_clim_only = [s.replace("pet", "E") for s in   x_tick_clim_only ]
  
  # Tick mark name of month without env. variable (upper x-axis)
  x_tick_month_only = [int(m[-2:]) for m in x_tick_names_sorted]
  prev = ["prev " if "prev" in p else "" for p in x_tick_names_sorted]
  Month = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
  x_tick_month_only = [a + Month[b-1] for a,b in zip(prev, x_tick_month_only)]
  del prev, Month

  '''Heatmap data array:
  --------------------------------------------------------------------------'''
  shap_heatmap_data= dict()
  max_shap_sd = 0 # will be updated in the loop; user for color bar max value
  # compute the standard deviations and save them in the matrix/pandas DF
  for i,scenario in enumerate(all_scenario_names):
    print(scenario)
    shap_heatmap =  pd.DataFrame(np.zeros(shape= [len(y_tick_ecoregion_names )] +  [len(clim_var_i)] , dtype=float))
    shap_heatmap.index = y_tick_ecoregion_names 
    for key in shap_values.keys():
      if scenario in key: 
        shap_sd = np.std( shap_values[key]["shap"][:,clim_var_i], axis=0)
        shap_heatmap.loc[ key.split(" ",1)[1] ] = shap_sd
    
    shap_heatmap = shap_heatmap.iloc[:,x_tick_order] # sort x-axis
    shap_heatmap_data[scenario] = shap_heatmap # append matrix to dict with all scenrio's data
    # check if new maximum values is
    max_temp = max(shap_heatmap)
    if max_shap_sd < max_temp:
      max_shap_sd = max_temp
  
  del(shap_heatmap, max_temp)
  # round max value up to the next dec
  max_shap_sd = np.round(max_shap_sd,-1)

  ## Standardized version of the shap values (per Ecoregion)
  shap_heatmap_scaled = copy.deepcopy(shap_heatmap_data)
  max_shap_sd_scaled = 0 # will be updated in the loop; user for color bar max value
  for key in shap_heatmap_scaled.keys():
    for i in range(shap_heatmap_scaled[key].shape[0]):
      shap_heatmap_scaled[key].iloc[i,:] =  shap_heatmap_data[key].iloc[i,:] / np.std( shap_heatmap_data[key].iloc[i,:] )
      max_temp = max(shap_heatmap_scaled[key].iloc[i,:])
      if max_temp > max_shap_sd_scaled:
        max_shap_sd_scaled = max_temp
        
  max_shap_sd_scaled = np.ceil(max_shap_sd_scaled)

  ''' Plotting the heat maps for each scenario and time period
  --------------------------------------------------------------------------'''
  
  # vertical lines for the month
  x_grid = np.linspace(start=3, stop=shap_heatmap_data[key].shape[1] - 3, num=int(shap_heatmap_data[key].shape[1]/3) - 1)  
  #
  all_x_ticks_env = np.array(range(len(x_tick_names))) + 0.5
  all_x_ticks_month = np.linspace(start=1.5, stop= len(x_tick_names) - 1.5, num= int(len(x_tick_names) / 3 ) )
  all_y_ticks = np.array(range(len(y_tick_ecoregion_names))) + 0.5
  
  
  
  print("Plottin the absolute and standardized ShAP effects")
  for scenario in all_scenario_names:
    scenario_name_as_title = scenario.replace(".gcm", "")
    scenario_name_as_title = scenario_name_as_title.replace("13GCMs_ensemble_","")
    scenario_name_as_title = scenario_name_as_title.replace("_", " ")
    
    ''' a) Absolute (mm²) data plot '''
    if not standardized_color_scale: # use color (shap SD) range for all scenarios or just this one
      max_shap_sd = np.round(np.max( shap_heatmap_data[scenario].to_numpy()), -1)
      
    plt.rcParams.update({'font.size': 8})
    #### Panel figure with axes:
    fig, axes = plt.subplots(nrows=4, dpi=300, figsize=(15/2.54, 45/2.54))
    ax = axes[0]
    ax.set_title("a - Absoulte SHAP values - " + scenario_name_as_title)
    # Raw value plot
    heatmap_a = ax.pcolormesh(shap_heatmap_data[scenario], vmin=0, vmax=max_shap_sd)
    fig.colorbar(mappable= heatmap_a, ax=ax, location="bottom",
                 label="Standard deviation of the variables' effect on BAI in mm²",
                 shrink=0.5, pad=0.2
                 )
    ax.set_xticks(all_x_ticks_env, x_tick_clim_only, rotation=0, fontsize=6)
    ax.set_xlim(0,60)
    ax.set_yticks(all_y_ticks, y_tick_ecoregion_names)
    ax.vlines(x=x_grid, ymin=0, ymax= len(shap_heatmap_data[scenario]), colors="white" )
    # second x-axis with month
    ax2 = ax.twiny()
    ax2.set_xlim(0,60)
    ax2.set_xticks( 
      np.linspace(start=1.5, stop= len(x_tick_month_only) - 1.5, num= int(len(x_tick_month_only) / 3 ) ),
                   x_tick_month_only[1::3] , rotation=70)
    
    ''' b) Standardized per Ecoregions'''
    ax = axes[1]
    ax.set_title("b - Standardized SHAP values - " + scenario_name_as_title)
    heatmap_b = ax.pcolormesh(shap_heatmap_scaled[scenario], vmin=0, vmax=max_shap_sd_scaled)
    fig.colorbar(mappable= heatmap_b, ax=ax, location="bottom",
                 label="Standard deviation of the variables' effect on BAI\n(standardized per Ecoregion)",
                 shrink=0.5, pad=0.2
                 )
    ax.set_xticks(all_x_ticks_env, x_tick_clim_only, rotation=0, fontsize=6)
    ax.set_yticks(all_y_ticks, y_tick_ecoregion_names)#, rotation='vertical')
    ax.vlines(x=x_grid, ymin=0, ymax= len(shap_heatmap_scaled[scenario]), colors="white" )
    # second x-axis with month
    ax2 = ax.twiny()
    ax2.set_xlim(0,60)
    ax2.set_xticks(all_x_ticks_month, x_tick_month_only[1::3] , rotation=70)
    
    
    ''' c) Heat maps for the change of the ShAP values, relative to the baseline'''
    # print("Plottin the absolute change in the ShAP effects")
    # for scenario in all_scenario_names:
    shap_heatmap_diff = shap_heatmap_data[scenario] - shap_heatmap_data["1961-1990"]
    ax = axes[2]
    ax.set_title("c- SHAP change relative to baseline - " + scenario_name_as_title)
    heatmap_c = ax.pcolormesh(shap_heatmap_diff,
                            vmin=-max_shap_sd, vmax=max_shap_sd,
                            cmap="RdBu"
                            )
    fig.colorbar(mappable= heatmap_c, ax=ax, location="bottom",
                 label="Change in the standard deviation of the variables' effect on BAI in mm²\n(relative to the baseline scenario)",
                 shrink=0.5)
    
    ax.set_xticks(all_x_ticks_env, x_tick_clim_only, rotation=0, fontsize=6)
    ax.set_yticks(all_y_ticks, y_tick_ecoregion_names)#, rotation='vertical')
    ax.vlines(x=x_grid, ymin=0, ymax= len(shap_heatmap_scaled[scenario]), colors="black" )
    # second x-axis with month
    ax2 = ax.twiny()
    ax2.set_xlim(0,60)
    ax2.set_xticks(all_x_ticks_month, x_tick_month_only[1::3] , rotation=70)
    
    ''' d) Heat maps for the change of the ShAP standardized values, relative to the baseline'''
    # print("Plottin the standardized change in the ShAP effects")
    # for scenario in all_scenario_names:
    shap_heatmap_diff_scaled = shap_heatmap_scaled[scenario] - shap_heatmap_scaled["1961-1990"]
    ax = axes[3]
    ax.set_title("d - SHAP change relative to baseline - " + scenario_name_as_title)
    heatmap_d = ax.pcolormesh(shap_heatmap_diff_scaled,
                            vmin=-max_shap_sd_scaled, vmax=max_shap_sd_scaled,
                            cmap="RdBu"
                            )
    fig.colorbar(mappable= heatmap_d, ax=ax, location="bottom",
                 label="Change in the standard deviation of the variables' effect on BAI in mm²\n(standardized per Ecoregion and relative to the baseline scenario)",
                 shrink=0.5)
    
    ax.set_xticks(all_x_ticks_env, x_tick_clim_only, rotation=0, fontsize=6)
    ax.set_yticks(all_y_ticks, y_tick_ecoregion_names)
    ax.vlines(x=x_grid, ymin=0, ymax= len(shap_heatmap_scaled[scenario]), colors="black" )
    # second x-axis with month
    ax2 = ax.twiny()
    ax2.set_xlim(0,60)
    ax2.set_xticks(all_x_ticks_month, x_tick_month_only[1::3] , rotation=70)

    plt.subplots_adjust(hspace=0.6)
    plt.savefig(save_dir + scenario + ".jpg", bbox_inches='tight')
    plt.pause(0.1)
    plt.clf()
    plt.close()
    gc.collect()
    
    del heatmap_a, heatmap_b, heatmap_c, heatmap_d
    
    
    
    ''' Alternative plots as line graphs per ecoregion '''
    # temp_i = np.array(list(range(shap_heatmap_data["1961-1990"].shape[1]))[0::3])
    # ppt_i = temp_i + 1
    # pet_i = temp_i + 2

    # for i in range(len(shap_heatmap_data["1961-1990"])):  
    #   fig, axes = plt.subplots(nrows=3, dpi=150, figsize=(16.5, 12))  
    #   ax = axes[0]
    #   ax.set_title(shap_heatmap_data["1961-1990"].index[i] + "\nTemperature")
    #   ax.plot(shap_heatmap_data["1961-1990"].iloc[i,temp_i].values, color="red")
    #   ax.set_ylim(0,30)
      
    #   ax = axes[1]
    #   ax.set_title("ppt")
    #   ax.plot(shap_heatmap_data["1961-1990"].iloc[i,ppt_i].values, color="blue")
    #   ax.set_ylim(0,30)
      
    #   ax = axes[2]
    #   ax.set_title("pet")
    #   ax.plot(shap_heatmap_data["1961-1990"].iloc[i,pet_i].values, color="purple")
    #   ax.set_ylim(0,30)
      
    #   fig.tight_layout()
    #   plt.show()
       

'''-------------------------------------------------------------------------'''
def unit_from_var_name(x):
  """ BACKEND fucntion. Returns the measurement unit from variable names,
  e.g. 'in mm' 
  Like a dictionary, but can also check if a string occurs within another.
  e.g: "pet" occurs within "prev_pet07" """
  if x=="ba":
    return " in mm²"
  if x=="rw":
    return " in mm"
  if "pet" in x:
    return " in mm"
  if "ppt" in x:
    return " in mm"
  if "tave" in x:
    return " in °C"
  if x=="cra":
    return " in years"
  else:
    return ""
