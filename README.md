<p align="center">
  <img src="Figures/logo_ml_Pigl.png" width="200" class="center">
</p>
<center> <h1>Machine learning model for white spruce</h1> </center>



This repository contains the machine learning model to estimate the growth rate (basal area increment, BAI) of white spruce in North America.  
The model uses the following inputs:   

* individual tree-ring width measurements  
* monthly climate data (temperature, precipitation and potential evapotranspiration)  
* cambial ring age and stem diameter (when the respective tree-ring was formed)  

The model is a boosted regression tree based on scikit-learn and the hyperparameters were tuned in a grid search.  
Model evaluated is done with ShAP values to understand what the most important drivers of tree growth are.  
White spruce growth rated and ShAP values are computed for different:  

- climate change scenarios
- time periods
- Ecoregions


<img src="Figures/BRT ring width predictions.jpg" alt="drawing" width="600"/>
<img src="Figures/BRT chronology predictions.jpg" alt="drawing" width="600"/>

Model accuracy for individual tree-ring predictions (a)and when the predictions are averaged per site and year.
  
  
<img src="Figures/Manuscript_Shap_Heatmap_20230815.jpg" alt="drawing" width="1200"/>

The heatmaps (a) and (b) show which monthly climate variables affected BAI the most in the reference period (1961-1990) within each Ecoregion. In (c), the relative change in the variables importance between the reference period and and strong climate change scenario ssp585 is shown.
  
For more details please read the manuscript/article.