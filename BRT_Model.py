#!/usr/bin/env python3
# -*- coding: utf-8 -*-
''' --------------------------------------------------------------------------
Deep tree
Model 04 - Boosted regression tree
---------------------------------------------------------------------------'''

from sklearn.ensemble import GradientBoostingRegressor as BRT
import numpy as np

def data_prep_m4(data, clim_var_names, tree_var_names, return_names=False):
  '''Create one big dataframe with the needed columns for the BRT model'''
  all_names = clim_var_names + tree_var_names
  column_i = []
  # find all the right columns based on variable names
  for substring in all_names:
    column_i.append( [substring in string for string in data.columns.values] )    
  column_i = np.logical_or.reduce((column_i))
  # exclude current year month from September to December:
  exclude_i = []
  for month in ['Sep','Oct','Nov','Dec']:
    month_i = [month in string for string in data.columns.values]
    prev_i = ["prev" not in string for string in data.columns.values]
    exclude_i.append( np.logical_and(month_i, prev_i) )
  exclude_i = np.logical_or.reduce((exclude_i))
  column_i = np.logical_and(column_i, ~exclude_i)
  
  new_columns = list(data.columns.values[column_i])
  if "annual_ppt" in data.columns:
    new_columns.remove("annual_ppt")
  new_data = data[ new_columns ]
  column_names = new_data.columns
  
  if return_names:
    return(new_data, column_names)
  else:
    return(new_data)
  
  
def build_model_4(train_data, train_labels, train_weights,
                  validation_data, validation_labels, validation_weights,
                  clim_var_names, tree_var_names):
  ''' Build and train the BRT model '''
  model = BRT(loss='squared_error',
               learning_rate=0.1, n_estimators=27,
                min_samples_split = 128,
                max_depth = 16,
                verbose = 1
                )

  train_data_formatted = data_prep_m4(train_data, clim_var_names, tree_var_names)
  validation_data_formatted = data_prep_m4(validation_data, clim_var_names, tree_var_names)
  
  model.fit(train_data_formatted, train_labels, train_weights)
  # Predict train and validation data
  # train_predict = model.predict(train_data_formatted)
  # validation_predict = model.predict(validation_data_formatted)
  # Calculate R² score:.reset_index()
  train_score = model.score(train_data_formatted, train_labels , sample_weight=train_weights)
  validation_score = model.score(validation_data_formatted, validation_labels, sample_weight=validation_weights)  
  print("Train R² score: " + str(train_score) )
  print("Validation R² score: " + str(validation_score) )
  
  return model


''' Hyper parameter optimization of the BRT via halfing grid search
---------------------------------------------------------------------------'''
from sklearn.experimental import enable_halving_search_cv
from sklearn.model_selection import HalvingGridSearchCV

def build_model_4h(train_data, train_labels, train_weights,
                  validation_data, validation_labels, validation_weights,
                  clim_var_names, tree_var_names):
  model = BRT(loss='squared_error')
  
  train_data_formatted = data_prep_m4(train_data, clim_var_names, tree_var_names)

  param_grid = {"max_depth": [4,8,16,32,64],
                "learning_rate": [0.1, 0.03, 0.01],
                "min_samples_split": [64,128,512,1024]
                }

  search = HalvingGridSearchCV(model, param_grid, resource='n_estimators',
                                max_resources=30,
                                n_jobs = 4,
                                verbose=2).fit(train_data_formatted,
                                                    train_labels,
                                                    train_weights)
  best_model = search.best_estimator_
  return best_model, search