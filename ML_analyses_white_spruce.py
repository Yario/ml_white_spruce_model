# -*- coding: utf-8 -*-
"""###########################################################################

Machine learning tree-growth models for white spruce in North America

############################################################################"""

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import os
import gc
from datetime import datetime
import pickle
from copy import deepcopy
## Sub-scripts with evaluation functions, models, etc.
os.chdir("/home/mario/Dokumente/Python/deep_tree/ML_white_spruce/")
import Model_evaluation as mev # Custom functions for the model evaluation


"""###########################################################################
Settings
###########################################################################"""
## Select growth rate variable (target):
'''RW - ring width'''
# target_name = "rw"
# target_unit = "mm" 
# tree_var_names = ['cra', 'crw', 'latitude', 'elevation']

'''BAI - basal area increment'''
target_name = "bai" 
target_unit = "mm²"
tree_var_names = ['cra', 'ba', 'latitude', 'elevation']

## Climate  variables
clim_var_names = ['tave', 'ppt', 'pet']


"""###########################################################################
Section 1) Data
###########################################################################"""

'''Loading data with pandas'''
## read data, as fomatted and saved as csv with the R-script:
df = pd.read_csv("input/deep_tree_input_2023_04_26.csv")

future_climate = pd.read_csv("input/deep_tree_climate_change_data_2023_04_26.csv", low_memory=False)

''' Omit outliers '''
i = np.where(df.bai < 10000)[0]
df = df.iloc[i,:]

''' Depending on selected target variable: Remove BAI and BA -- OR -- RW and CRW '''
if target_name == "bai":
  df = df.drop(["rw", "crw"], axis=1)
if target_name == "rw":
  df = df.drop(["bai", "ba"], axis=1)
  
df = df.drop(['rw_prev', 'bai_prev', 'longitude'], axis=1)

''' Load level II ecoregions and create dictionary
----------------------------------------------------------------------------'''
# site_cluster = pd.read_csv("input/Sites with Ecoregions.csv")
site_cluster = pd.read_csv("input/Sites with Ecoregions_20230428.csv")
# Add Number of sites (N) to each Ecoregion's name:
region_counts = site_cluster.EcoRegion2.value_counts()
for i in range(len(region_counts)):
  new_name = region_counts.keys()[i] + " (" + str(region_counts[i]) + ")"
  j = site_cluster.EcoRegion2 == region_counts.keys()[i]
  site_cluster.loc[j,"EcoRegion2"] = new_name

all_sites = pd.Series(df["site"]).unique()
# all_regions =  site_cluster['Cluster2'].unique()
all_regions =  site_cluster['EcoRegion2'].unique()
all_regions = list(filter(lambda v: v==v, all_regions)) # remove nan
# site_region_dict = dict(zip(site_cluster["SiteCode"], site_cluster["Cluster2"]))
site_region_dict = dict(zip(site_cluster["SiteCode"], site_cluster["EcoRegion2"]))
df["Ecoregion"] = [ site_region_dict.get(key) for key in df["site"] ]
# convert dataframe with site name, PI and region into a dictionary, used for plot labels
site_dict = site_cluster.copy()
site_dict = site_dict.set_index("SiteCode").T.to_dict('list')
del site_cluster, site_region_dict, new_name, j, i


''' Compute mean site climate variables
----------------------------------------------------------------------------'''
df.groupby(by=["site","year"])
site_means = df.drop_duplicates(["site","year"]).groupby("site").mean()
# site_means.columns = [x.replace("prev_", "") for x in site_means.columns]
''' July temperature '''
mean_tave07 = pd.DataFrame({"mean_tave07":site_means.tave07,
                            "site":site_means.index})
mean_tave07 = mean_tave07.reset_index(drop=True)
df = df.merge(mean_tave07, how="left", on="site")

''' May temperature '''
mean_tave05 = pd.DataFrame({"mean_tave05":site_means.tave05,
                            "site":site_means.index})
mean_tave05 = mean_tave05.reset_index(drop=True)
df = df.merge(mean_tave05, how="left", on="site")

''' mean annual ppt sum '''
ppt_columns = ["prev_ppt" + "%02d" % (x+1,) for x in range(12)] # use previous years data columns because current years misses Okt-Dec
ppt_sum = site_means[ppt_columns].sum(axis=1)
ppt_sum = pd.DataFrame({"site": ppt_sum.index, "mean_ppt_sum":ppt_sum})
ppt_sum = ppt_sum.reset_index(drop=True)
df = df.merge(ppt_sum, how="left", on="site")

''' mean annual pet sum '''
pet_columns = ["prev_pet" + "%02d" % (x+1,) for x in range(12)] # use previous years data columns because current years misses Okt-Dec
pet_sum = site_means[pet_columns].sum(axis=1)
pet_sum = pd.DataFrame({"site": pet_sum.index, "mean_pet_sum":pet_sum})
pet_sum = pet_sum.reset_index(drop=True)
df = df.merge(pet_sum, how="left", on="site")

# tree_var_names = tree_var_names + [               "mean_tave07", "mean_ppt_sum", "mean_pet_sum"]
tree_var_names = tree_var_names + ["mean_tave05", "mean_tave07", "mean_ppt_sum", "mean_pet_sum"]

del ppt_sum, ppt_columns, pet_sum, pet_columns, site_means, mean_tave07, mean_tave05

''' Same computations (Compute mean site climate variables)
for the future climate data'''
site_means = future_climate.drop_duplicates(["site","year"]).groupby("site").mean()
# site_means.columns = [x.replace("prev_", "") for x in site_means.columns]
''' July temperature '''
mean_tave07 = pd.DataFrame({"mean_tave07":site_means.tave07,
                            "site":site_means.index})
mean_tave07 = mean_tave07.reset_index(drop=True)
future_climate = future_climate.merge(mean_tave07, how="left", on="site")
''' May temperature '''
mean_tave05 = pd.DataFrame({"mean_tave05":site_means.tave05,
                            "site":site_means.index})
mean_tave05 = mean_tave05.reset_index(drop=True)
future_climate = future_climate.merge(mean_tave05, how="left", on="site")
''' mean annual ppt sum '''
ppt_columns = ["prev_ppt" + "%02d" % (x+1,) for x in range(12)] # use previous years data columns because current years misses Okt-Dec
ppt_sum = site_means[ppt_columns].sum(axis=1)
ppt_sum = pd.DataFrame({"site": ppt_sum.index, "mean_ppt_sum":ppt_sum})
ppt_sum = ppt_sum.reset_index(drop=True)
future_climate = future_climate.merge(ppt_sum, how="left", on="site")
''' mean annual pet sum '''
pet_columns = ["prev_pet" + "%02d" % (x+1,) for x in range(12)] # use previous years data columns because current years misses Okt-Dec
pet_sum = site_means[pet_columns].sum(axis=1)
pet_sum = pd.DataFrame({"site": pet_sum.index, "mean_pet_sum":pet_sum})
pet_sum = pet_sum.reset_index(drop=True)
future_climate = future_climate.merge(pet_sum, how="left", on="site")

del ppt_sum, ppt_columns, pet_sum, pet_columns, site_means, mean_tave07


''' Split data into train/validation/test data based on randomly sampled years.
----------------------------------------------------------------------------'''
#  (Nearby sites will have very similar climate and similar growth rates.
#   Using completely random samples (tree-rings) instead of years would thus
#   lead to over-fitting due to spatial autocorrelation.)
df_years = df['year'].unique()
split_fractions = [0.7,0.15,0.15] #fractions of train, validation and test data

''' Create randomly split years for train, test and validation: '''
train_years, test_years = train_test_split(
  df_years,
  test_size=split_fractions[2],
  random_state=42
  )
train_years, validation_years = train_test_split(
  train_years,
  test_size=split_fractions[1] / (split_fractions[0] + split_fractions[1]),
  random_state=42
  )

train_years.sort()
validation_years.sort()
test_years.sort()

''' Creating df indices (boolean) for each subset '''
train_i = df['year'].isin(train_years)
validation_i = df['year'].isin(validation_years)
test_i = df['year'].isin(test_years)
# creating separate dataframe for each subset
train_data = df[train_i]
validation_data = df[validation_i]
test_data = df[test_i]

'''Split the data in labels, independent variables and meta data '''
# Labels
train_labels = train_data.pop(target_name)
validation_labels = validation_data.pop(target_name)
test_labels = test_data.pop(target_name)
# split off the meta-data that is not used for training
metad_columns = pd.Series(['year', 'tree', 'site','Ecoregion'])
metad_columns = metad_columns[ np.isin(metad_columns, train_data.columns.values) ]

#Meta data; Train:
train_metad = train_data[metad_columns].copy()
train_data = train_data.drop(metad_columns, axis=1)
# add id column to have a column with unique values for each tree
train_metad['id'] = train_metad['site'].astype(str) + train_metad['tree'].astype(str)
#Validation:
validation_metad = validation_data[metad_columns].copy()
validation_data = validation_data.drop(metad_columns, axis=1)
validation_metad['id'] = validation_metad['site'].astype(str) + validation_metad['tree'].astype(str)
#Test:
test_metad = test_data[metad_columns].copy()
test_data = test_data.drop(metad_columns, axis=1)
test_metad['id'] = test_metad['site'].astype(str) + test_metad['tree'].astype(str)


train_stats = train_data.describe().transpose() # used for SHAP plots


''' no Feb pet values (0 or NaN) -> replace with 0 to avoid error
----------------------------------------------------------------------------'''
train_data = train_data.replace(np.nan, 0) 
train_data = train_data.replace(np.inf, 0) 
validation_data = validation_data.replace(np.nan, 0)
validation_data = validation_data.replace(np.inf, 0)
test_data = test_data.replace(np.nan, 0)
test_data = test_data.replace(np.inf, 0)

''' Calculate sample weights
----------------------------------------------------------------------------'''
# (The number of trees and rings per site differs a lot. Thus, each sample is
# given a weight so that all sites have equal importance during training.)
sw = 1 / train_metad['site'].value_counts() / len(np.unique(train_metad['site'].astype(str)))
sw = dict(zip(sw.index.values, sw.values))
train_weights = np.array([ sw.get(key) for key in train_metad["site"] ])
validation_weights = np.array([ sw.get(key) for key in validation_metad["site"] ])
test_weights = np.array([ sw.get(key) for key in test_metad["site"] ])
# sum(train_weights) # should be ~1

## Cleaning up and freeing RAM
del df
gc.collect()

"""###########################################################################
Section 2) Build and train the model
###########################################################################"""

''' Train boosted regression tree (8)model-4b)
---------------------------------------------------------------------------'''
from BRT_Model import build_model_4
from BRT_Model import data_prep_m4
''' Build and train BRT model (4b)'''
# model_4b = build_model_4(train_data, train_labels, train_weights,
#                     validation_data, validation_labels, validation_weights,
#                     clim_var_names, tree_var_names)

''' Save BRT model (4b) '''
# now = datetime.now().strftime("%Y_%m_%d__%H_%M_%S")
# pkl_name = "BRT_Model_4b_TEST_WITH_2023_DATA_" + now
# with open("saved_models/" + pkl_name + ".pkl", 'wb') as file:
#     pickle.dump(model_4b, file)


''' Load  BRT model (4b) '''
with open(
    "saved_models/BRT_Model_4bh_TEST_WITH_2023_DATA_2023_04_27__13_50_00.pkl"
  ,'rb') as file:
  model_4b = pickle.load(file)


''' Model 4b (BRT) hyper-parameter optimization '''
# from BRT_Model import build_model_4h
# model_4bh, search = build_model_4h(train_data, train_labels, train_weights,
#                   validation_data, validation_labels, validation_weights,
#                   clim_var_names, tree_var_names)

# results from 22.11.2021, as well as 27.04.2023 with ClimateNA data and BAI:
# GradientBoostingRegressor(learning_rate=0.1, max_depth=16, min_samples_split=128, n_estimators=27)

''' Save model_4b after hyperparameter optimization'''
# now = datetime.now().strftime("%Y_%m_%d__%H_%M_%S")
# pkl_name = "BRT_Model_4bh_TEST_WITH_2023_DATA_" + now
# with open("saved_models/" + pkl_name + ".pkl", 'wb') as file:
#     pickle.dump(model_4bh, file)

'''Save search file from the hyperparamter optimization'''  
# pkl_name_search = "BRT_Model_4bh_searchData_TEST_WITH_2023_DATA" + now
# with open("saved_models/" + pkl_name_search + ".pkl", 'wb') as file:
#     pickle.dump(search, file)

'''Load search data from hyperparameter optimization'''
# with open(
#     "saved_models/BRT_Model_4bh_searchData_TEST_WITH_2023_DATA2023_04_27__13_50_00.pkl"
#   ,'rb') as file:
#   search = pickle.load(file)

# model_4b = search.best_estimator_
# model_4bh = model_4b
# search.best_params_

gc.collect()

"""###########################################################################
Section 3) Evaluating the models
###########################################################################"""

''' Reformatting data for evaluation '''
all_labels= pd.concat([train_labels, validation_labels, test_labels], ignore_index=True)

# all_metad = train_metad.append(validation_metad, ignore_index=True).append(test_metad, ignore_index=True)
all_metad = pd.concat([train_metad, validation_metad, test_metad], ignore_index=True)
all_weights = np.concatenate((train_weights, validation_weights, test_weights), axis=0)
all_data = pd.concat([train_data, validation_data,test_data], ignore_index=True)

# add column which indicates what is train/val/test data
all_metad['subset'] = ""
train_i = all_metad['year'].isin(train_years)
validation_i = all_metad['year'].isin(validation_years)
test_i = all_metad['year'].isin(test_years)
all_metad.loc[train_i,'subset'] = 0
all_metad.loc[validation_i,'subset'] = 1
all_metad.loc[test_i,'subset'] = 2

''' BRT predictions '''

all_predictions_brt = model_4b.predict( data_prep_m4(all_data, clim_var_names, tree_var_names) )
# individual tree-rings to site chronologies:
chron_brt = mev.ring_to_chron(labels=all_labels, predictions=all_predictions_brt,
                      metad=all_metad, prediction_name="BRT_predictions")

''' Plot random tree predictions (simple visual check)
---------------------------------------------------------------------------'''
# mind different y-scales and individual trees have lots of noise
mev.plot_tree_predictions(
    labels=all_labels,
    predictions = all_predictions_brt,
    metad=all_metad
    )

''' Plot histograms of the climate variables in each time period and scenario
    to assess to what extent the new data is outside the training data and thus
    an extrapolation into the unknown.
---------------------------------------------------------------------------'''
input_var_names = train_data.columns
thresholds = []
# Loop over all the trees in the ensemble and extract the threshold values
for stage, stage_trees in enumerate(model_4b.estimators_):
    for tree in stage_trees:
        for feature, threshold in zip(tree.tree_.feature, tree.tree_.threshold):
            if feature >= 0:
                variable_name = input_var_names[feature]
                thresholds.append([stage+1, variable_name, threshold])
# Convert to dataframe
decision_thresholds = pd.DataFrame(thresholds, columns=['stage', 'variable_name', 'threshold'])

for var_name in ["tave07", "ppt07", "pet07"]:
  mev.plot_clim_hist_over_time(
    save_dir = "Figures/",
    future_climate = future_climate,
    var_name = var_name,
    decision_thresholds = decision_thresholds
    )

''' Compute ShAP values for all sites separately
---------------------------------------------------------------------------'''
''' Compute (mind the long computation time)'''
N_shap =5000# 50#200#5000 # number of samples for SHAP

''' Compute SHAP values '''
# ''' global BRT (model 4) '''
# shap_values_m4b_global = mev.compute_shap(
#   model = model_4b, data = data_prep_m4(train_data, clim_var_names,tree_var_names, return_names=True),
#   interpretability="global",
#   site = train_metad.site, N = N_shap, sample_weights = train_weights)

# ''' local BRT (Model 4) '''
# shap_values_m4b_local = mev.compute_shap(
#   model = model_4b, data = data_prep_m4(train_data, clim_var_names, tree_var_names, return_names=True),
#   interpretability="local",
#   site = train_metad.site, N = N_shap, sample_weights = train_weights)


# # Save SHAP values
# now = datetime.now().strftime("%Y_%m_%d-%H_%M_%S")
# with open('saved_shap_values/shap_values_' + now + '.pkl', 'wb') as file:
#     # pickle.dump((shap_values_m2b_global, shap_values_m2b_local, shap_values_m4b_global, shap_values_m4b_local), file)
#     pickle.dump((shap_values_m4b_global, shap_values_m4b_local), file)

''' Load computed SHAP values '''
with open('saved_shap_values/shap_values_2023_04_27-21_05_39.pkl', "rb") as f:
    shap_values_m4b_global, shap_values_m4b_local = pickle.load(f) #2023 version, no ANN anymore
    


''' Plot global SHAP values
----------------------------------------------------------------------------'''
## BRT Model - M4b:
mev.plot_shap(shap_values_m4b_global, "Figures/SHAP_BRT_global.jpg", title="a",
              x_label="SHAP value (effect on BAI in mm²)")
mev.plot_shap(shap_values_m4b_global, "Figures/SHAP_BRT_global_monthly_climate.jpg",
              title="b", omit_var = tree_var_names,
              x_label="SHAP value (effect on BAI in mm²)") 


''' Compute SHAP values for different subsets/cluster of the data to
compare variable importance
----------------------------------------------------------------------------'''
''' Compute '''
# cluster_shap_values = dict()
# # for var_name in ["cra", "ba", "latitude", "Ecoregion", "mean_tave07", "mean_tave05", "mean_ppt_sum" ,"mean_pet_sum"]:
# for var_name in ["Ecoregion"]:
# # for var_name in ["mean_ppt_sum" ,"mean_pet_sum"]:
#   print("Computing variable: " + var_name)
#   cluster_shap_values[var_name] = mev.shap_subset(all_data, all_metad, all_weights,
#               model = model_4b, data_prep = data_prep_m4,
#               N=N_shap, 
#               var_name = var_name, quantile_breaks = [0.1,0.3,0.5,0.7,0.9],
#               clim_var_names=clim_var_names, tree_var_names=tree_var_names,
#               return_shap_values = True, save_dir="SHAP_cluster_plots/"
#               )

# # Save SHAP values for each ecoregion
# now = datetime.now().strftime("%Y_%m_%d-%H_%M_%S")
# with open('saved_shap_values/cluster_shap_values_' + now + '.pkl', 'wb') as file:
#     pickle.dump(cluster_shap_values, file)

''' Load computed ShAP values'''
with open('saved_shap_values/cluster_shap_values_2023_04_28-00_15_22.pkl', "rb") as f:
    cluster_shap_values = pickle.load(f)


''' Plot swarm plots for selected environmental variables and the different clusters'''
mev.shap_swarm_plot(
  x=cluster_shap_values,
  unstandardize = False,
  train_stats = train_stats,
  target_name = target_name, target_unit = target_unit,
  selected_variabes = ["ba", "cra", "prev_pet07", "prev_pet06", "prev_tave07", "mean_tave05", "mean_tave07", "elevation", "mean_ppt_sum", "mean_pet_sum"],
  save_dir = "SHAP_cluster_plots/",
  alpha=0.3
  )

''' Measured vs. predicted
----------------------------------------------------------------------------'''
# Chronology level (mean per site and year)
mev.plot_prediction_vs_truth(chron_brt.BRT_predictions, chron_brt.labels,
                                   chron_brt.subset,
                                   xlab= target_name.upper() + " in " + target_unit,
                                   ylab= "Predicted " + target_name.upper() + " in " + target_unit,
                                   title="b",
                                   filename="Figures/BRT chronology predictions.jpg"
                                   )

# Individual tree-rings
mev.plot_prediction_vs_truth(pd.Series(all_predictions_brt),
                             all_labels,
                             all_metad.subset,
                             xlab= target_name.upper() + " in " + target_unit,
                             ylab= "Predicted " + target_name.upper() + " in " + target_unit,
                             title="a",
                             filename="Figures/BRT ring width predictions.jpg"
                             )


''' Plot model evaluation per site
----------------------------------------------------------------------------'''
now = datetime.now().strftime("%Y_%m_%d__%H_%M_%S")
## One pdf page per site with multiple plots
mev.plot_site_eval(d = chron_brt,#all_chron,
                    method =  'all',# 'example' or 'all'
                    # method =  'example',
                    file_name = "Figures/Complete_site_predictions_" + now +"_" + target_name.upper() + "_ylim_quantile_95_Ecoregions.pdf",
                    shap_values_m2b_local = None,#shap_values_m2b_local,
                    shap_values_m4b_local = shap_values_m4b_local,
                    omit_var_names = tree_var_names,
                    target_name = target_name, target_unit=target_unit,
                    site_dict = site_dict,
                    )

''' Combine tree-size classes with future climate data to predict growth rates
----------------------------------------------------------------------------'''
new_data = mev.fixed_tree_size(
  data = all_data,
  metad = all_metad,
  # ,size = [25,50,75,100,150,200,250,300] # used for diameter (for stem diameter/crw )
  size = [1000,5000,10000,15000,30000,60000,100000, 140000], # used for basal area = BA
  train_stats = train_stats,
  new_climate_data = future_climate,
  normalize=False # after the ANN was deprecated, standarditation was not needed anymore
  )

''' Growth trends for all sites combined '''
m4_cc_predictions = model_4b.predict(
    data_prep_m4(new_data[0], clim_var_names, tree_var_names) )

mev.plot_climate_change_growth_trends(
  predictions=m4_cc_predictions
  ,new_metad=new_data[1]
  ,target_name = target_name
  ,filename="Figures/BRT_climat_change_predictions.jpg" )


''' Plot the growth trends for each ecoregion
----------------------------------------------------------------------------'''
new_data[1]["ecoregion"] = ""
# add ecoregion to each data point
for i,site in enumerate(new_data[1].site):
  new_data[1].loc[i,"ecoregion"] = site_dict[site][7] # quite slow, speed up!

# loop over each ecoregion and create the respective plot
for region in all_regions:
  region_i = new_data[1].ecoregion == region # subset index for the ecoregion
  mev.plot_climate_change_growth_trends(
    predictions = m4_cc_predictions[region_i]
    ,new_metad = new_data[1][region_i]
    ,target_name = target_name
    ,filename="Figures/BRT_climat_change_predictions_"+ region + ".jpg",
    title=region, ylim=[0,5000]
    )

del(i, site)

''' ShAP values for different future time periods (all sites combined)
----------------------------------------------------------------------------'''
all_scenarios = new_data[1].period.unique()
site = None

''' Loop over all periods and climate change scenarios'''
for scenario in all_scenarios:
  print(scenario)
  '''Create data subset for the scenario and time period '''
  subset_i = new_data[1].period == scenario
  if site is not None: # if only data from a singel site shall be used
    subset_i = np.logical_and(subset_i, new_data[1].site == site)
  formatted_data_subset = data_prep_m4(new_data[0][subset_i], clim_var_names,tree_var_names, return_names=True)
  scenario_predictions = model_4b.predict(formatted_data_subset[0])
  scenario_weights = np.array([ sw.get(key) for key in new_data[1][subset_i].site ])
  
  '''Compute SHAP values and plot '''
  temp_shap = mev.compute_shap(
    model = model_4b, data = formatted_data_subset ,
    interpretability="global", return_explainer=False,
    N = N_shap, sample_weights = scenario_weights)
  
  ## Create plot title
  if scenario ==  '1961-1990':
      plot_title = scenario
  else:
    plot_title = scenario.split("_")[2] + " " + scenario.split("_")[3].split(".")[0]
    
  mev.plot_shap(shap_values = deepcopy(temp_shap), # deepcopy needed =/
                filename = "Figures/SHAP_" + scenario[:-4]+".jpg",
                title = plot_title,
                x_label="SHAP value (effect on BAI in mm²)"
                )
  mev.plot_shap(shap_values = deepcopy(temp_shap),
                filename = "Figures/SHAP_" + scenario[:-4] + "_monthly_clim.jpg",
                title = plot_title,
                x_label="SHAP value (effect on BAI in mm²)",
                omit_var = tree_var_names
                )
  


'''ShAP heatmap for each
  a)ecoregion, 
  b)climate change scenario,
  c)time period and
  d)change in the ShAP values relative to baseline
----------------------------------------------------------------------------'''

''' Compute the ShAP values for each data subset'''
# # 14 region * 13 scenarios and periods = 182 data sub-sets! Takes a while...
# # N_shap = 5000
# ''' Loop over all periods and climate change scenarios'''
# subset_shap = dict()
# for scenario in all_scenarios:
#   # scenario = all_scenarios[0]
#   ''' Loop over all Ecoregions'''
#   for region in all_regions:
#     # region = all_regions[0]
#     print(str(len(subset_shap)+1) + "/" + str(14*13) + " --- " + scenario +  " --- " + region)    
#     '''Create data subset for the scenario and time period '''
#     subset_i = np.logical_and(new_data[1].period == scenario, 
#                               new_data[1].ecoregion == region
#                               )
#     subset_length = sum(subset_i) 
#     # format the datasubst for the BRT model 
#     formatted_data_subset = data_prep_m4(new_data[0][subset_i], clim_var_names,tree_var_names, return_names=True)
#     scenario_predictions = model_4b.predict(formatted_data_subset[0])
#     scenario_weights = np.array([ sw.get(key) for key in new_data[1][subset_i].site ])

#     '''Compute SHAP values and plot '''
#     # Sample size for the SHAP computation is set to a max of N_Shap = 5000
#     if len(subset_i) < N_shap:
#       N_shap_subset = None#len(subset_i)
#     else:
#       N_shap_subset = N_shap
    
#     # temp_shap = mev.compute_shap(
#     subset_shap[scenario + " " +  region]= mev.compute_shap(
#       model = model_4b, data = formatted_data_subset,
#       interpretability="global", return_explainer=False,
#       N = N_shap, sample_weights = scenario_weights)

# ''' Save the computed subset_shap values '''
# now = datetime.now().strftime("%Y_%m_%d__%H_%M_%S")
# pkl_name = "Subset_Shap_values_" + now
# with open("saved_shap_values/" + pkl_name + ".pkl", 'wb') as file:
#     pickle.dump(subset_shap, file)

''' Load the computed subset_shap values '''
with open('saved_shap_values/Subset_Shap_values_2023_07_05__21_51_33.pkl', "rb") as f:
    subset_shap = pickle.load(f)
    
''' Plot the ShAP heatmaps  '''    
mev.shap_heatmap_scenario_ecoregion(
  save_dir="Figures/ShAP_ecoregion_scenario_plots/",
  shap_values=subset_shap,
  tree_var_names = tree_var_names,
  clim_var_names = clim_var_names,
  standardized_color_scale = True # same axis extent for all scenarios
  )


  
''' Histogram of the R² frequency distributions on a site level
----------------------------------------------------------------------------'''
site_r2 = mev.siteR2(file_name= 'Figures/Site_chronology_R2_subsets.jpg',
                     d=chron_brt)



